package uz.pdp.bot;


//Asadbek Xalimjonov 12/21/21 8:58 AM

import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.pdp.bot.service.AmazonBot;

import static uz.asad.util.Util.CYAN;
import static uz.asad.util.Util.print;

public class Main {

    public void botStart() {

        try {
            TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);
            api.registerBot(new AmazonBot());
            print(CYAN, "BOT STARTED");
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
