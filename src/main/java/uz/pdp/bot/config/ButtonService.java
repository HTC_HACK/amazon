package uz.pdp.bot.config;


import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import uz.pdp.model.abtract.User;
import uz.pdp.model.user.Customer;

import static uz.asad.util.Util.*;


//Asadbek Xalimjonov 12/21/21 9:22 AM

public interface ButtonService {
    ReplyKeyboard getreplyKeyboard(User customer);
}
